var strength = {
    0: "Mieux vaudrait éviter ⛔",
    1: "Mauvais 💣",
    2: "Faible 👎",
    3: "Bon 👍",
    4: "Fort 💪"
}

var password = document.getElementById('password');
var meter = document.getElementById('password-strength-meter');
var text = document.getElementById('password-strength-text');
var feedback = document.getElementById('feedback');
var qualite = document.getElementById('qualite_v');
var info_warn_container = document.getElementById('info_w');
var info_warn = document.getElementById('info_warn');
var info_suggestion_container = document.getElementById('info_s');
var info_suggestion = document.getElementById('info_suggestion');


password.addEventListener('input', function() {
    var val = password.value;
    var result = zxcvbn(val);
    // Update the text indicator
    if(val !== "") {

        // Update the password strength meter
        meter.value = result.score;

        qualite.innerHTML=strength[result.score];

        document.getElementById('info_crakedin').innerHTML=result.crack_times_display.offline_fast_hashing_1e10_per_second;
        
        info_warn.innerHTML=result.feedback.warning;
        if (result.feedback.warning != "") {
            info_warn_container.style.visibility='visible';
        } else {
            info_warn_container.style.visibility='hidden';
        }
        info_suggestion.innerHTML=result.feedback.suggestions; 
        if (result.feedback.warning != "") {
            info_suggestion_container.style.visibility='visible';
        } else {
            info_suggestion_container.style.visibility='hidden';
        }

        feedback.style.visibility="visible";
    }
    else {
        feedback.style.visibility="hidden";
        qualite.innerHTML = "";
        document.getElementById('info_warn').innerHTML = "";
        document.getElementById('info_suggestion').innerHTML = "";
    }
});