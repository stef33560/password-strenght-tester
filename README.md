# Password Strength tester
Cette page s'éxécute intégralement coté client, 100% javascript, et a pour but de vérifier la robustesse d'un mot de passe.

Le script exploite la bibliothèque [dropbox/zxcvbn](https://github.com/dropbox/zxcvbn) afin de vérifier la robustesse et d'indiquer des conseils supplémentaires.

# Dependences
La [bibliothèque javascript zxcvbn](https://gitlab.forge.education.gouv.fr/stef33560/password-strenght-tester/-/blob/main/js/zxcvbn.js) se récupère dans le module NPM idoine. Pour la récupérer :
```bash
mkdir -p /tmp/node_modules
npm install --prefix /tmp/node_modules zxcvbn
```

Les fichiers à récupérer dans `/tmp/node_modules/zxcvbn/dist/` sont `zxcvbn.js` et  `zxcvbn.js.map`, placés ici dans le dossier [`js/`](https://gitlab.forge.education.gouv.fr/stef33560/password-strenght-tester/-/tree/main/js).

# Exécution
Cloner le dépot et le présenter avec NGINX
```bash
git clone git@gitlabssh.forge.education.gouv.fr:stef33560/password-strenght-tester.git
cd password-strenght-tester
docker run --name nginx -v $PWD:/usr/share/nginx/html -p 8080:80 -d nginx
```

Allez avec votre navigateur favori sur `http://localhost:8080`

![Démarrage](doc/password-strenght-tester-00.png)

![Saisie d'un mot de passe](doc/password-strenght-tester-01.png)
